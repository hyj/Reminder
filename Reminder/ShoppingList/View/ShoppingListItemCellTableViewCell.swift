//
//  ShoppingListItemCellTableViewCell.swift
//  Reminder
//
//  Created by Jason on 2017/9/10.
//  Copyright © 2017年 Iowa State University Com S 309. All rights reserved.
//

import UIKit
import CoreData

class ShoppingListItemCellTableViewCell: UITableViewCell,BEMCheckBoxDelegate {

    var Item:ShoppingListItem?


    

    @objc func MarkDone()
    {
        Item!.done = !Item!.done
        
        
        
        if(!(Item?.UpdateToServer())!)
        {
            print("Cannt Update To Server")
        }
        
        Item?.updateDate=Date()
        Item?.updateDate?.addTimeInterval(-60*60*5)
        
        
        try? AppDelegate.PersistentContainer.viewContext.save()
        
        
    }
    private var context=AppDelegate.PersistentContainer.viewContext
    
    @IBOutlet weak var ItemName: UILabel!
    
    @IBOutlet weak var NeedBy: UILabel!
    
    @IBOutlet weak var AmountAndUnit: UILabel!
    {
        didSet{
            AmountAndUnit.alpha = 0.6
        }
    }
    
    @IBOutlet weak var CheckMark: BEMCheckBox!
    {
        didSet{
            CheckMark.delegate=self
            
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    func didTap(_ checkBox: BEMCheckBox) {
        MarkDone()
    }

    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
