//
//  DesignableView.swift
//  Reminder
//
//  Created by Jason on 2017/10/1.
//  Copyright © 2017年 Iowa State University Com S 309. All rights reserved.
//

import UIKit

@IBDesignable class DesignableView: UIView {

    @IBInspectable var BoarderColor:UIColor = UIColor.clear{
        didSet{
            self.layer.borderColor=BoarderColor.cgColor
        }
    }
    
    @IBInspectable var CornerRadius:CGFloat=0{
        didSet{
            self.layer.cornerRadius=CornerRadius
        }
    }
    
    @IBInspectable var BorderWidth:CGFloat=0{
        didSet{
            self.layer.borderWidth=BorderWidth
        }
    }
    

}
